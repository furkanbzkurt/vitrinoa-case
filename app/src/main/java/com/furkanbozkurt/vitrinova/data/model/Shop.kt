package com.furkanbozkurt.vitrinova.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Shop(
    @SerializedName("comment_count")
    val commentCount: Int,
    val cover: Cover?,
    @SerializedName("created_at")
    val createdAt: String,
    val definition: String,
    @SerializedName("follower_count")
    val followerCount: Int,
    val id: Int,
    @SerializedName("is_editor_choice")
    val isEditorChoice: Boolean,
    @SerializedName("is_following")
    val isFollowing: Boolean,
    val logo: Logo?,
    val name: String,
    @SerializedName("name_updateable")
    val nameUpdateable: Boolean,
    @SerializedName("popular_products")
    val popularProducts : List<Product>?,
    @SerializedName("product_count")
    val productCount: Int,
    @SerializedName("share_url")
    val shareUrl: String,
    @SerializedName("shop_payment_id")
    val shopPaymentId: Int,
    @SerializedName("shop_rate")
    val shopRate: Int,
    val slug: String,
    @SerializedName("vacation_mode")
    val vacationMode: Int
) : Parcelable