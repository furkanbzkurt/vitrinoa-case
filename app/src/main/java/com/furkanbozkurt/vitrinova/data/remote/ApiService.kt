package com.furkanbozkurt.vitrinova.data.remote

import com.furkanbozkurt.vitrinova.data.model.DiscoverPageResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("discover")
    suspend fun getDiscoverPageData(): DiscoverPageResponse

}