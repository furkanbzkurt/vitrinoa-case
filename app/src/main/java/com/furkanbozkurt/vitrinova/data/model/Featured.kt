package com.furkanbozkurt.vitrinova.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Featured(
    val cover: Cover,
    val id: Int,
    @SerializedName("sub_title")
    val subTitle: String,
    val title: String,
    val type: String
) : Parcelable