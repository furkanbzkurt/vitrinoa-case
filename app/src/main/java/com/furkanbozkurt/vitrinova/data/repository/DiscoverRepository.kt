package com.furkanbozkurt.vitrinova.data.repository

import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.data.model.DiscoverPageResponse
import com.furkanbozkurt.vitrinova.util.State
import kotlinx.coroutines.flow.Flow

interface DiscoverRepository {
    suspend fun fetchDiscoverPageData(): Flow<State<List<DiscoverItem>>>
}