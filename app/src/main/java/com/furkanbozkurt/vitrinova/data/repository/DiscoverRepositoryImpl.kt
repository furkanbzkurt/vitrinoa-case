package com.furkanbozkurt.vitrinova.data.repository

import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.data.remote.ApiService
import com.furkanbozkurt.vitrinova.util.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class DiscoverRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : DiscoverRepository {

    override suspend fun fetchDiscoverPageData(): Flow<State<List<DiscoverItem>>> {
        return flow<State<List<DiscoverItem>>> {
            emit(State.loading())
            val responseDiscoverPageData = apiService.getDiscoverPageData()
            if (responseDiscoverPageData.size > 0) {
                emit(State.success(responseDiscoverPageData))
            } else {
                emit(State.error("An error occurred."))
            }
        }.flowOn(Dispatchers.IO)
            .catch { e ->
                emit(State.error("An error occurred."))
            }
    }
}