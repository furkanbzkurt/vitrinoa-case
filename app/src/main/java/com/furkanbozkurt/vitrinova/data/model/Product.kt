package com.furkanbozkurt.vitrinova.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    @SerializedName("cargo_time")
    val cargoTime: Int,
    val category: Category,
    @SerializedName("category_id")
    val categoryId: Int,
    @SerializedName("comment_count")
    val commentCount: Int,
    @SerializedName("commission_rate")
    val commissionRate: Int,
    val definition: String,
    val difference: String,
    val id: Int,
    val images: List<Image>?,
    @SerializedName("is_active")
    val isActive: Boolean,
    @SerializedName("is_approved")
    val isApproved: Boolean,
    @SerializedName("is_cargo_free")
    val isCargoFree: Boolean,
    @SerializedName("is_editor_choice")
    val isEditorChoice: Boolean,
    @SerializedName("is_liked")
    val isLiked: Boolean,
    @SerializedName("is_new")
    val isNew: Boolean,
    @SerializedName("is_owner")
    val isOwner: Boolean,
    @SerializedName("like_count")
    val likeCount: Int,
    @SerializedName("old_price")
    val oldPrice: Int,
    val price: Int,
    @SerializedName("share_url")
    val shareUrl: String,
    val shop: Shop?,
    val slug: String,
    val stock: Int,
    val title: String
) : Parcelable