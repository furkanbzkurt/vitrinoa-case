package com.furkanbozkurt.vitrinova.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParentCategory(
    val id: Int,
    val name: String,
    val order: Int,
    @SerializedName("parent_category")
    val parentCategory: ParentCategory?,
    @SerializedName("parent_id")
    val parentId: Int?
) : Parcelable