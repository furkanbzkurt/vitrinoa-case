package com.furkanbozkurt.vitrinova.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Collection(
    val cover: Cover?,
    val definition: String,
    val id: Int,
    val logo: Logo,
    @SerializedName("share_url")
    val shareUrl: String,
    val start: String,
    val title: String
) : Parcelable