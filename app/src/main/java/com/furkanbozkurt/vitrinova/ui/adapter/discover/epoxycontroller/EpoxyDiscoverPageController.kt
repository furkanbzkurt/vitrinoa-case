package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxycontroller

import android.graphics.Color
import android.view.Gravity
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.TypedEpoxyController
import com.airbnb.epoxy.carousel
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel.*
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.Constants.Companion.CATEGORIES
import com.furkanbozkurt.vitrinova.util.Constants.Companion.COLLECTIONS
import com.furkanbozkurt.vitrinova.util.Constants.Companion.EDITOR_SHOPS
import com.furkanbozkurt.vitrinova.util.Constants.Companion.FEATURED
import com.furkanbozkurt.vitrinova.util.Constants.Companion.NEW_PRODUCTS
import com.furkanbozkurt.vitrinova.util.Constants.Companion.NEW_SHOPS
import com.furkanbozkurt.vitrinova.util.setSlideInRightAnimation
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper


class EpoxyDiscoverPageController(private val discoverPageEventListener: DiscoverPageEventListener) :
    TypedEpoxyController<List<DiscoverItem>>() {
    override fun buildModels(data: List<DiscoverItem>) {
        data.forEach { discoverItem ->
            when (discoverItem.type) {
                FEATURED -> addFeaturedSliders(discoverItem)
                NEW_PRODUCTS -> addNewProducts(discoverItem)
                CATEGORIES -> addCategories(discoverItem)
                COLLECTIONS -> addCollections(discoverItem)
                EDITOR_SHOPS -> addEditorShops(discoverItem)
                NEW_SHOPS -> addNewShops(discoverItem)
            }
        }
    }

    private fun addFeaturedSliders(discoverItem: DiscoverItem) {
        featuredShops {
            id(FEATURED)
            discoverItem(discoverItem)
            listener(discoverPageEventListener)
            spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
        }
    }

    private fun addNewProducts(discoverItem: DiscoverItem) {
        newProductsHeader {
            id(NEW_PRODUCTS)
            discoverItem(discoverItem)
            listener(discoverPageEventListener)
            spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
        }

        discoverItem.products?.let { productList ->
            val newProductItemModels = productList.map {
                NewProductItemModel_()
                    .id(it.id)
                    .newProduct(it)
                    .listener(discoverPageEventListener)
            }

            carousel {
                onBind { model, recyclerView, position ->
                    recyclerView.setSlideInRightAnimation()
                }
                id(NEW_PRODUCTS)
                initialPrefetchItemCount(3)
                hasFixedSize(true)
                padding(Carousel.Padding.dp(12, 8, 12, 16, 8))
                models(newProductItemModels)
                spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
            }
        }
    }

    private fun addCategories(discoverItem: DiscoverItem) {
        categoriesHeader {
            id(CATEGORIES)
            discoverItem(discoverItem)
            spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
        }

        discoverItem.categories?.let { categoryList ->
            val categoryItemModels = categoryList.map {
                CategoryItemModel_()
                    .id(it.id)
                    .category(it)
                    .listener(discoverPageEventListener)
            }

            carousel {
                onBind { model, recyclerView, position ->
                    recyclerView.setBackgroundColor(Color.parseColor("#f4f4f4"))
                    GravitySnapHelper(Gravity.START)
                        .attachToRecyclerView(recyclerView)
                    recyclerView.setSlideInRightAnimation()
                }
                hasFixedSize(true)
                id(CATEGORIES)
                padding(Carousel.Padding.dp(12, 8, 12, 16, 4))
                models(categoryItemModels)
                spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
            }
        }
    }

    private fun addCollections(discoverItem: DiscoverItem) {
        collectionsHeader {
            id(COLLECTIONS)
            discoverItem(discoverItem)
            listener(discoverPageEventListener)
            spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
        }

        discoverItem.collections?.let { collectionList ->
            val collectionItemModels = collectionList.map {
                CollectionItemModel_()
                    .id(it.id)
                    .collectionItem(it)
                    .listener(discoverPageEventListener)
            }

            carousel {
                onBind { model, recyclerView, position ->
                    GravitySnapHelper(Gravity.START).attachToRecyclerView(recyclerView)
                }
                id(COLLECTIONS)
                initialPrefetchItemCount(3)
                hasFixedSize(true)
                padding(Carousel.Padding.dp(12, 8, 12, 16, 4))
                models(collectionItemModels)
                spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
            }
        }
    }

    /**
     * Bu method diğerlerinden ayrı olmak zorundadır çünkü tasarımda beklenen durum sadece
     * carousel ve hader'dan ibaret değildir. Background image setlenmesi gerektiği için ayrı bir
     * Epoxy controller'ına gerekli veriler gönderilir.
     *
     */

    private fun addEditorShops(discoverItem: DiscoverItem) {
        discoverItem.shops?.let {
            editorShops {
                id(EDITOR_SHOPS)
                discoverItem(discoverItem)
                listener(discoverPageEventListener)
                spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
            }
        }
    }

    private fun addNewShops(discoverItem: DiscoverItem) {
        newShopsHeader {
            id(NEW_SHOPS)
            discoverItem(discoverItem)
            listener(discoverPageEventListener)
            spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
        }

        discoverItem.shops?.let { shopList ->
            val newShopItemModels = shopList.map {
                NewShopItemModel_()
                    .id(it.id)
                    .newShopItem(it)
                    .listener(discoverPageEventListener)
            }

            carousel {
                onBind { model, recyclerView, position ->
                    GravitySnapHelper(Gravity.CENTER).attachToRecyclerView(recyclerView)
                }
                id(NEW_SHOPS)
                initialPrefetchItemCount(3)
                hasFixedSize(true)
                models(newShopItemModels)
                spanSizeOverride { _totalSpanCount, _, _ -> return@spanSizeOverride _totalSpanCount }
            }
        }
    }
}