package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_collections_header)
abstract class CollectionsHeaderModel : EpoxyModelWithHolder<CollectionsHeaderModel.Holder>() {

    @EpoxyAttribute
    lateinit var discoverItem: DiscoverItem

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.apply {
            textViewNCollectionsHeader.text = discoverItem.title
            textViewShowAllCollections.setOnClickListener {
                listener.onCollectionsHeaderClick(discoverItem = discoverItem)
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val textViewNCollectionsHeader by bind<TextView>(R.id.textViewNCollectionsHeader)
        val textViewShowAllCollections by bind<TextView>(R.id.textViewShowAllCollections)
    }
}