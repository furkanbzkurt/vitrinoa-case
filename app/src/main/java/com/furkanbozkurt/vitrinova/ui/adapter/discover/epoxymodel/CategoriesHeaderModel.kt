package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_categories_header)
abstract class CategoriesHeaderModel : EpoxyModelWithHolder<CategoriesHeaderModel.Holder>() {

    @EpoxyAttribute
    lateinit var discoverItem: DiscoverItem

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.apply {
            textViewCategoriesHeader.text = discoverItem.title
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val textViewCategoriesHeader by bind<TextView>(R.id.textViewCategoriesHeader)
    }
}