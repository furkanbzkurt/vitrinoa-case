package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import coil.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.Shop
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder
import com.furkanbozkurt.vitrinova.util.hide
import com.furkanbozkurt.vitrinova.util.show
import com.google.android.material.card.MaterialCardView

@EpoxyModelClass(layout = R.layout.item_new_shop)
abstract class NewShopItemModel : EpoxyModelWithHolder<NewShopItemModel.Holder>() {

    @EpoxyAttribute
    lateinit var newShopItem: Shop

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun bind(holder: Holder) {
        super.bind(holder)
        with(newShopItem) {
            holder.apply {
                cardViewNewShopItem.setOnClickListener {
                    listener.onNewShopItemClick(shop = newShopItem)
                }
                imageViewNewShop.load(cover?.medium?.url)
                imageViewNewShopLogo.load(logo?.medium?.url)
                if (logo != null) {
                    imageViewNewShopLogo.show()
                    textViewNewShopLogoPlaceholder.hide()
                } else {
                    imageViewNewShopLogo.hide()
                    textViewNewShopLogoPlaceholder.text = name.substring(0, 1)
                    textViewNewShopLogoPlaceholder.show()
                }
                textViewNewShopTitle.text = name
                textViewNewShopDescription.text = definition
                textViewNewShopProductCount.text = "$productCount ÜRÜN"
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val cardViewNewShopItem by bind<MaterialCardView>(R.id.cardViewNewShopItem)
        val imageViewNewShop by bind<AppCompatImageView>(R.id.imageViewNewShop)
        val imageViewNewShopLogo by bind<AppCompatImageView>(R.id.imageViewNewShopLogo)
        val textViewNewShopLogoPlaceholder by bind<TextView>(R.id.textViewNewShopLogoPlaceholder)
        val textViewNewShopTitle by bind<TextView>(R.id.textViewNewShopTitle)
        val textViewNewShopDescription by bind<TextView>(R.id.textViewNewShopDescription)
        val textViewNewShopProductCount by bind<TextView>(R.id.textViewNewShopProductCount)
    }
}