package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.data.model.Product
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_new_products_header)
abstract class NewProductsHeaderModel : EpoxyModelWithHolder<NewProductsHeaderModel.Holder>() {

    @EpoxyAttribute
    lateinit var discoverItem: DiscoverItem

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.apply {
            textViewNewProductsHeader.text = discoverItem.title
            textViewShowAllNewProducts.setOnClickListener {
                listener.onNewProductHeaderClick(discoverItem = discoverItem)
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val textViewNewProductsHeader by bind<TextView>(R.id.textViewNewProductsHeader)
        val textViewShowAllNewProducts by bind<TextView>(R.id.textViewShowAllNewProducts)
    }
}