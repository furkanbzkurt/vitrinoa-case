package com.furkanbozkurt.vitrinova.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.navigation.navArgs
import com.furkanbozkurt.vitrinova.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CollectionListActivity : AppCompatActivity() {

    private val args: ProductListActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collection_list)

        val textView = findViewById<TextView>(R.id.textViewDummy)
        args.discoverItem.collections?.let { collectionList ->
            for (i in collectionList.indices) {
                textView.text = "${textView.text} \n ${collectionList[i].title}"
            }
        }
    }
}