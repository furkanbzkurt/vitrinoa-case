package com.furkanbozkurt.vitrinova.ui.adapter.discover.featuredpageradapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.furkanbozkurt.vitrinova.data.model.Featured
import com.furkanbozkurt.vitrinova.databinding.ItemFeaturedShopBinding
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener


class FeaturedShopsViewPagerAdapter(
    private val discoverPageEventListener: DiscoverPageEventListener
) : ListAdapter<Featured, RecyclerView.ViewHolder>(FeaturedShopItemDiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FeaturedShopItemViewHolder(
            ItemFeaturedShopBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val featuredShopItem = getItem(position)
        (holder as FeaturedShopItemViewHolder).bind(featuredShopItem)
    }


    inner class FeaturedShopItemViewHolder(
        private val binding: ItemFeaturedShopBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Featured) {
            binding.apply {
                featuredItem = item
                executePendingBindings()
            }

            itemView.setOnClickListener {
                discoverPageEventListener.onFeaturedSlideItemClick(item)
            }
        }
    }
}

private class FeaturedShopItemDiffCallback : DiffUtil.ItemCallback<Featured>() {

    override fun areItemsTheSame(oldItem: Featured, newItem: Featured): Boolean {
        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: Featured, newItem: Featured): Boolean {
        return oldItem == newItem
    }
}
