package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import coil.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.Product
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder
import com.furkanbozkurt.vitrinova.util.drawLineOverText
import com.furkanbozkurt.vitrinova.util.show
import com.google.android.material.card.MaterialCardView

@EpoxyModelClass(layout = R.layout.item_new_product)
abstract class NewProductItemModel : EpoxyModelWithHolder<NewProductItemModel.Holder>() {

    @EpoxyAttribute
    lateinit var newProduct: Product

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener


    override fun bind(holder: Holder) {
        super.bind(holder)
        with(newProduct) {
            holder.apply {
                cardViewNewProductItem.setOnClickListener {
                    listener.onNewProductItemClick(product = newProduct)
                }
                imageViewNewProduct.load(images?.get(0)?.thumbnail?.url)
                textViewNewProductTitle.text = title
                textViewNewProductShopName.text = shop?.name
                if (oldPrice != 0) {
                    textViewNewProductPrice.drawLineOverText()
                    textViewNewProductPrice.text = "$oldPrice TL"
                    textViewNewProductPrice.show()
                }
                textViewNewProductCurrentPrice.text = "$price TL"
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val cardViewNewProductItem by bind<MaterialCardView>(R.id.cardViewNewProductItem)
        val imageViewNewProduct by bind<AppCompatImageView>(R.id.imageViewNewProduct)
        val textViewNewProductTitle by bind<TextView>(R.id.textViewNewProductTitle)
        val textViewNewProductShopName by bind<TextView>(R.id.textViewNewProductShopName)
        val textViewNewProductPrice by bind<TextView>(R.id.textViewNewProductPrice)
        val textViewNewProductCurrentPrice by bind<TextView>(R.id.textViewNewProductCurrentPrice)
    }
}