package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxycontroller

import com.airbnb.epoxy.TypedEpoxyController
import com.furkanbozkurt.vitrinova.data.model.Shop
import com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel.editorShopItem
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener

class EpoxyEditorShopsController(private val discoverPageEventListener: DiscoverPageEventListener) :
    TypedEpoxyController<List<Shop>>() {
    override fun buildModels(editorShopList: List<Shop>) {
        editorShopList.forEach {
            editorShopItem {
                id(it.id)
                editorShop(it)
                listener(discoverPageEventListener)
            }
        }
    }
}