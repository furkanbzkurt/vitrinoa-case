package com.furkanbozkurt.vitrinova.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs
import com.furkanbozkurt.vitrinova.R
import dagger.hilt.android.AndroidEntryPoint

@SuppressLint("SetTextI18n")
@AndroidEntryPoint
class ShopListActivity : AppCompatActivity() {

    private val args: ProductListActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop_list)

        val textView = findViewById<TextView>(R.id.textViewDummy)
        args.discoverItem.shops?.let { shopList ->
            for (i in shopList.indices) {
                textView.text = "${textView.text} \n ${shopList[i].name}"
            }
        }
    }
}