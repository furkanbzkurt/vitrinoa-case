package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import coil.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.Category
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder
import com.google.android.material.card.MaterialCardView

@EpoxyModelClass(layout = R.layout.item_category)
abstract class CategoryItemModel : EpoxyModelWithHolder<CategoryItemModel.Holder>() {

    @EpoxyAttribute
    lateinit var category: Category

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun bind(holder: Holder) {
        super.bind(holder)
        with(category) {
            holder.apply {
                cardViewCategoryItem.setOnClickListener {
                    listener.onCategoryItemClick(category = category)
                }
                imageViewCategory.load(logo?.thumbnail?.url)
                textViewCategoryName.text = name
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val cardViewCategoryItem by bind<MaterialCardView>(R.id.cardViewCategoryItem)
        val imageViewCategory by bind<AppCompatImageView>(R.id.imageViewCategory)
        val textViewCategoryName by bind<TextView>(R.id.textViewCategoryName)
    }
}