package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import coil.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.Collection
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder
import com.google.android.material.card.MaterialCardView

@EpoxyModelClass(layout = R.layout.item_collection)
abstract class CollectionItemModel : EpoxyModelWithHolder<CollectionItemModel.Holder>() {

    @EpoxyAttribute
    lateinit var collectionItem: Collection

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun bind(holder: Holder) {
        super.bind(holder)
        with(collectionItem) {
            holder.apply {
                cardViewCollectionItem.setOnClickListener {
                    listener.onCollectionItemClick(collection = collectionItem)
                }
                imageViewCollection.load(cover?.medium?.url)
                textViewCollectionTitle.text = title
                textViewCollectionDescription.text = definition
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val cardViewCollectionItem by bind<MaterialCardView>(R.id.cardViewCollectionItem)
        val imageViewCollection by bind<AppCompatImageView>(R.id.imageViewCollection)
        val textViewCollectionTitle by bind<TextView>(R.id.textViewCollectionTitle)
        val textViewCollectionDescription by bind<TextView>(R.id.textViewCollectionDescription)
    }
}