package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.LinearSnapHelper
import coil.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.epoxy.EpoxyRecyclerView
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxycontroller.EpoxyEditorShopsController
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder
import com.furkanbozkurt.vitrinova.util.OnSnapPositionChangeListener
import com.furkanbozkurt.vitrinova.util.attachSnapHelperWithListener

@EpoxyModelClass(layout = R.layout.layout_editor_shops)
abstract class EditorShopsModel : EpoxyModelWithHolder<EditorShopsModel.Holder>() {

    @EpoxyAttribute
    lateinit var discoverItem: DiscoverItem

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun shouldSaveViewState(): Boolean {
        return true
    }

    override fun bind(holder: Holder) {
        super.bind(holder)
        discoverItem.shops?.let { shopList ->
            val epoxyEditorShopsController = EpoxyEditorShopsController(listener)
            holder.textViewEditorShopsTitle.text = discoverItem.title
            holder.textViewShowAllEditorShops.setOnClickListener {
                listener.onEditorShopsHeaderClick(discoverItem = discoverItem)
            }
            val snapHelper = LinearSnapHelper()
            holder.epoxyRecyclerViewEditorShops.attachSnapHelperWithListener(
                snapHelper = snapHelper,
                onSnapPositionChangeListener = object :
                    OnSnapPositionChangeListener {
                    override fun onSnapPositionChange(position: Int) {
                        holder.imageViewEditorShopBackground.load(shopList[position].cover?.medium?.url)
                    }
                })
            holder.epoxyRecyclerViewEditorShops.setController(epoxyEditorShopsController)
            holder.epoxyRecyclerViewEditorShops.setHasFixedSize(true)
            epoxyEditorShopsController.setData(shopList)
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val imageViewEditorShopBackground by bind<AppCompatImageView>(R.id.imageViewEditorShopBackground)
        val textViewEditorShopsTitle by bind<TextView>(R.id.textViewEditorShopsTitle)
        val textViewShowAllEditorShops by bind<TextView>(R.id.textViewShowAllEditorShops)
        val epoxyRecyclerViewEditorShops by bind<EpoxyRecyclerView>(R.id.epoxyRecyclerViewEditorShops)
    }
}
