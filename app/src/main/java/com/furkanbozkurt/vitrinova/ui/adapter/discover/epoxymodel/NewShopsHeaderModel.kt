package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_new_shops_header)
abstract class NewShopsHeaderModel : EpoxyModelWithHolder<NewShopsHeaderModel.Holder>() {

    @EpoxyAttribute
    lateinit var discoverItem: DiscoverItem

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.apply {
            textViewNewShopsHeader.text = discoverItem.title
            textViewShowAllNewShops.setOnClickListener {
                listener.onNewShopsHeaderClick(discoverItem = discoverItem)
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val textViewNewShopsHeader by bind<TextView>(R.id.textViewNewShopsHeader)
        val textViewShowAllNewShops by bind<TextView>(R.id.textViewShowAllNewShops)
    }
}