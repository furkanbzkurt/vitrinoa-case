package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import androidx.viewpager2.widget.ViewPager2
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.ui.adapter.discover.featuredpageradapter.FeaturedShopsViewPagerAdapter
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder
import com.furkanbozkurt.vitrinova.util.ParallaxPageTransformer
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator

@EpoxyModelClass(layout = R.layout.layout_featured_shops)
abstract class FeaturedShopsModel : EpoxyModelWithHolder<FeaturedShopsModel.Holder>() {

    @EpoxyAttribute
    lateinit var discoverItem: DiscoverItem

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun shouldSaveViewState(): Boolean {
        return true
    }

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.apply {
            val featuredShopsViewPagerAdapter = FeaturedShopsViewPagerAdapter(listener)
            viewPagerFeaturedShops.adapter = featuredShopsViewPagerAdapter
            featuredShopsViewPagerAdapter.submitList(discoverItem.featured)
            pagerIndicator.setViewPager2(holder.viewPagerFeaturedShops)
            val pageTransformer = ParallaxPageTransformer()
            holder.viewPagerFeaturedShops.setPageTransformer(pageTransformer)
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val viewPagerFeaturedShops by bind<ViewPager2>(R.id.viewPagerFeaturedShops)
        val pagerIndicator by bind<SpringDotsIndicator>(R.id.pagerIndicator)
    }
}