package com.furkanbozkurt.vitrinova.ui.fragment.discover

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.vitrinova.data.model.DiscoverItem
import com.furkanbozkurt.vitrinova.data.repository.DiscoverRepository
import com.furkanbozkurt.vitrinova.util.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@ExperimentalCoroutinesApi
@HiltViewModel
class DiscoverViewModel @Inject constructor(
    private val discoverRepository: DiscoverRepository
) : ViewModel() {

    private val _discoverPageContentLiveData = MutableLiveData<State<List<DiscoverItem>>>()
    val discoverPageContentLiveData: LiveData<State<List<DiscoverItem>>>
        get() = _discoverPageContentLiveData

    init {
        getDiscoverPageContent()
    }

    fun getDiscoverPageContent() {
        viewModelScope.launch {
            discoverRepository.fetchDiscoverPageData().collect {
                _discoverPageContentLiveData.postValue(it)
            }
        }
    }
}