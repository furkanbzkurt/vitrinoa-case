package com.furkanbozkurt.vitrinova.ui.fragment.discover

import android.app.Activity
import android.content.Intent
import android.speech.RecognizerIntent
import androidx.lifecycle.Observer
import com.airbnb.epoxy.Carousel
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.base.BaseFragment
import com.furkanbozkurt.vitrinova.data.model.*
import com.furkanbozkurt.vitrinova.data.model.Collection
import com.furkanbozkurt.vitrinova.databinding.FragmentDiscoverBinding
import com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxycontroller.EpoxyDiscoverPageController
import com.furkanbozkurt.vitrinova.util.*
import com.furkanbozkurt.vitrinova.util.Constants.Companion.REQUEST_CODE_TEXT_TO_SPEECH
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class DiscoverFragment : BaseFragment<DiscoverViewModel, FragmentDiscoverBinding>() {

    private var discoverPageEventListener by autoCleared<DiscoverPageEventListener>()
    private var epoxyDiscoverPageController by autoCleared<EpoxyDiscoverPageController>()

    override fun getContentView(): Int = R.layout.fragment_discover
    override fun getViewModelClass(): Class<DiscoverViewModel> = DiscoverViewModel::class.java

    override fun setupUI() {
        init()
        setSwipeToRefreshListener()
        setVoiceSearchListener()
        setDiscoverPageEventListener()
        setEpoxyRecyclerViewController()
        observeDiscoverPageContent()
        setRetryClickListener()
    }

    private fun init() {
        binding.toolbarApp.searchView.show()
    }

    private fun setSwipeToRefreshListener() {
        binding.swipeToRefreshLayoutDiscoverPage.setOnRefreshListener {
            binding.swipeToRefreshLayoutDiscoverPage.isRefreshing = false
            viewModel.getDiscoverPageContent()
        }
    }

    private fun setVoiceSearchListener() {
        binding.toolbarApp.buttonSearchWithVoice.setOnClickListener {
            startSpeechToTextScreen()
        }
    }

    private fun setDiscoverPageEventListener() {
        discoverPageEventListener = object : DiscoverPageEventListener {
            override fun onFeaturedSlideItemClick(featured: Featured) {
                showToast(featured.title)
            }

            override fun onNewProductHeaderClick(discoverItem: DiscoverItem) {
                navigateScreen(
                    directions = DiscoverFragmentDirections.actionDiscoverScreenToProductList(
                        discoverItem = discoverItem
                    )
                )
            }

            override fun onNewProductItemClick(product: Product) {
                showToast(product.title)
            }

            override fun onCategoryItemClick(category: Category) {
                showToast(category.name)
            }

            override fun onCollectionsHeaderClick(discoverItem: DiscoverItem) {
                navigateScreen(
                    directions = DiscoverFragmentDirections.actionDiscoverScreenToCollectionList(
                        discoverItem = discoverItem
                    )
                )
            }

            override fun onCollectionItemClick(collection: Collection) {
                showToast(collection.title)
            }

            override fun onEditorShopsHeaderClick(discoverItem: DiscoverItem) {
                navigateScreen(
                    directions = DiscoverFragmentDirections.actionDiscoverScreenToShopList(
                        discoverItem = discoverItem
                    )
                )
            }

            override fun onEditorShopItemClick(shop: Shop) {
                showToast(shop.name)
            }

            override fun onNewShopsHeaderClick(discoverItem: DiscoverItem) {
                navigateScreen(
                    directions = DiscoverFragmentDirections.actionDiscoverScreenToShopList(
                        discoverItem = discoverItem
                    )
                )
            }

            override fun onNewShopItemClick(shop: Shop) {
                showToast(shop.name)
            }

        }
    }

    private fun setEpoxyRecyclerViewController() {
        epoxyDiscoverPageController = EpoxyDiscoverPageController(discoverPageEventListener)
        binding.epoxyDiscoverPageRecyclerView.setController(epoxyDiscoverPageController)
        binding.epoxyDiscoverPageRecyclerView.setHasFixedSize(true)
    }

    private fun observeDiscoverPageContent() {
        viewModel.discoverPageContentLiveData.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is State.Loading -> {
                    setLoadingState(isLoading = true)
                }
                is State.Success -> {
                    showDiscoverPageContent(listDiscoverItems = state.data)
                    setLoadingState(isLoading = false)
                }
                is State.Error -> {
                    setLoadingState(isLoading = false, errorMessage = state.message)
                }
            }
        })
    }

    private fun showDiscoverPageContent(listDiscoverItems: List<DiscoverItem>?) {
        Carousel.setDefaultGlobalSnapHelperFactory(null)
        epoxyDiscoverPageController.setData(listDiscoverItems)
    }

    private fun setRetryClickListener() {
        binding.layoutNoData.buttonTryFetchAgain.setOnClickListener {
            viewModel.getDiscoverPageContent()
        }
    }

    private fun startSpeechToTextScreen() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "tr-TR")
        startActivityForResult(intent, REQUEST_CODE_TEXT_TO_SPEECH)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_TEXT_TO_SPEECH) {
            if (data != null && resultCode == Activity.RESULT_OK) {
                data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)?.let {
                    binding.toolbarApp.searchView.setQuery(it[0], true)
                }
            }
        }
    }
}