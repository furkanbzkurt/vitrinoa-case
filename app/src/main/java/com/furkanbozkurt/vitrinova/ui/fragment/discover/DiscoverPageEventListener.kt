package com.furkanbozkurt.vitrinova.ui.fragment.discover

import com.furkanbozkurt.vitrinova.data.model.*
import com.furkanbozkurt.vitrinova.data.model.Collection


interface DiscoverPageEventListener {

    fun onFeaturedSlideItemClick(featured: Featured)
    fun onNewProductHeaderClick(discoverItem: DiscoverItem)
    fun onNewProductItemClick(product: Product)
    fun onCategoryItemClick(category: Category)
    fun onCollectionsHeaderClick(discoverItem: DiscoverItem)
    fun onCollectionItemClick(collection: Collection)
    fun onEditorShopsHeaderClick(discoverItem: DiscoverItem)
    fun onEditorShopItemClick(shop: Shop)
    fun onNewShopsHeaderClick(discoverItem: DiscoverItem)
    fun onNewShopItemClick(shop: Shop)
}