package com.furkanbozkurt.vitrinova.ui.adapter.discover.epoxymodel

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import coil.load
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.furkanbozkurt.vitrinova.R
import com.furkanbozkurt.vitrinova.data.model.Shop
import com.furkanbozkurt.vitrinova.ui.fragment.discover.DiscoverPageEventListener
import com.furkanbozkurt.vitrinova.util.KotlinEpoxyHolder
import com.google.android.material.card.MaterialCardView

@EpoxyModelClass(layout = R.layout.item_editor_shop)
abstract class EditorShopItemModel : EpoxyModelWithHolder<EditorShopItemModel.Holder>() {

    @EpoxyAttribute
    lateinit var editorShop: Shop

    @EpoxyAttribute
    lateinit var listener: DiscoverPageEventListener

    override fun bind(holder: Holder) {
        super.bind(holder)
        with(editorShop) {
            holder.apply {
                cardViewEditorShopInfo.setOnClickListener {
                    listener.onEditorShopItemClick(shop = editorShop)
                }
                imageViewEditorShopLogo.load(logo?.thumbnail?.url)
                textViewEditorShopTitle.text = name
                textViewEditorShopDescription.text = definition
                popularProducts?.let {
                    imageViewEditorShopImageOne.load(it[0].images?.get(0)?.thumbnail?.url)
                    imageViewEditorShopImageTwo.load(it[1].images?.get(0)?.thumbnail?.url)
                    imageViewEditorShopImageThree.load(it[2].images?.get(0)?.thumbnail?.url)
                }
            }
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val cardViewEditorShopInfo by bind<MaterialCardView>(R.id.cardViewEditorShopInfo)
        val imageViewEditorShopLogo by bind<AppCompatImageView>(R.id.imageViewEditorShopLogo)
        val textViewEditorShopTitle by bind<TextView>(R.id.textViewEditorShopTitle)
        val textViewEditorShopDescription by bind<TextView>(R.id.textViewEditorShopDescription)
        val imageViewEditorShopImageOne by bind<AppCompatImageView>(R.id.imageViewEditorShopImageOne)
        val imageViewEditorShopImageTwo by bind<AppCompatImageView>(R.id.imageViewEditorShopImageTwo)
        val imageViewEditorShopImageThree by bind<AppCompatImageView>(R.id.imageViewEditorShopImageThree)
    }
}