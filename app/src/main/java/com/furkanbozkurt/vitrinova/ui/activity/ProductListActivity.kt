package com.furkanbozkurt.vitrinova.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs
import com.furkanbozkurt.vitrinova.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListActivity : AppCompatActivity() {

    private val args: ProductListActivityArgs by navArgs()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)

        val textView = findViewById<TextView>(R.id.textViewDummy)
        args.discoverItem.products?.let { productList ->
            for (i in productList.indices) {
                textView.text = "${textView.text} \n ${productList[i].title}"
            }
        }
    }
}