package com.furkanbozkurt.vitrinova.di

import android.content.Context
import com.furkanbozkurt.vitrinova.util.PreferenceHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(@ApplicationContext appContext: Context) =
        PreferenceHelper(appContext)

}