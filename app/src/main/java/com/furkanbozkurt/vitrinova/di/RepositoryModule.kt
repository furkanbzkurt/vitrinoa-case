package com.furkanbozkurt.vitrinova.di


import com.furkanbozkurt.vitrinova.data.repository.DiscoverRepository
import com.furkanbozkurt.vitrinova.data.repository.DiscoverRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindDiscoverRepository(
        discoverRepositoryImpl: DiscoverRepositoryImpl
    ): DiscoverRepository
}