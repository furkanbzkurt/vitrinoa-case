package com.furkanbozkurt.vitrinova.util

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import coil.load

object BindingAdapters {

    /**
     * Image resource'unun layout üzerinden Coil kütüphanesi ile setlendiği methodtur.
     *
     * @param imageView : AppCompatImageView
     * @param url : İlgili resim url'i
     *
     */

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun setImage(imageView: AppCompatImageView, url: String?) {
        if (!url.isNullOrEmpty()) {
            imageView.load(url) {
                //error()
            }
        }
    }

}