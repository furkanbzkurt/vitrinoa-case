package com.furkanbozkurt.vitrinova.util

import android.graphics.Paint
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.furkanbozkurt.vitrinova.R


fun Fragment.showToast(message: String) =
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

/**
 * RecyclerView'ın scroll edildiği sırada ekranda gösterilen item'ın indexini dönen methodtur
 *
 */

fun SnapHelper.getSnapPosition(recyclerView: RecyclerView): Int {
    val layoutManager = recyclerView.layoutManager ?: return RecyclerView.NO_POSITION
    val snapView = findSnapView(layoutManager) ?: return RecyclerView.NO_POSITION
    return layoutManager.getPosition(snapView)
}

/**
 * RecyclerView'ın snap helper eklenir ve scroll edilen item'ın indexi listener aracılığıyla
 * döndürülür
 *
 */

fun RecyclerView.attachSnapHelperWithListener(
    snapHelper: SnapHelper,
    behavior: SnapOnScrollListener.Behavior = SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
    onSnapPositionChangeListener: OnSnapPositionChangeListener
) {
    if (onFlingListener == null) {
        snapHelper.attachToRecyclerView(this)
        val snapOnScrollListener =
            SnapOnScrollListener(snapHelper, behavior, onSnapPositionChangeListener)
        addOnScrollListener(snapOnScrollListener)
    }
}

/**
 * RecyclerView itemlarının sağdan sola animasyonlu gelmesini sağlayan methodtur
 *
 */

fun RecyclerView.setSlideInRightAnimation() {
    val resId: Int = R.anim.layout_animation_slide_right
    val animation: LayoutAnimationController =
        AnimationUtils.loadLayoutAnimation(this.context, resId)
    layoutAnimation = animation
}

/**
 * Navigation component ile yeni ekrana navigation yapılmasını sağlar
 *
 */

fun Fragment.navigateScreen(directions: NavDirections) {
    findNavController().navigate(directions)
}

/**
 * İlgili textView'ın üzerinin çizili olmasını sağlar.
 *
 */

fun TextView.drawLineOverText() {
    this.paintFlags = this.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}
