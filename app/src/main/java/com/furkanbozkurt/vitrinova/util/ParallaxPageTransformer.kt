package com.furkanbozkurt.vitrinova.util

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.item_featured_shop.view.*

class ParallaxPageTransformer : ViewPager2.PageTransformer {

    override fun transformPage(view: View, position: Float) {
        val pageWidth: Int = view.width
        try {
            when {
                position < -1 -> view.alpha = 1f
                position <= 1 -> view.imageViewFeaturedShop.translationX = -position * (pageWidth / 1.5f)
                else -> view.alpha = 1f
            }
        } catch (e: Exception) {

        }

    }
}