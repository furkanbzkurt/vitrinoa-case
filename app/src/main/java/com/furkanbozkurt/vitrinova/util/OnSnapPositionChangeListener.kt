package com.furkanbozkurt.vitrinova.util

interface OnSnapPositionChangeListener {

    fun onSnapPositionChange(position: Int)
}