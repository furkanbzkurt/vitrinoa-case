package com.furkanbozkurt.vitrinova.util

import android.content.Context


class PreferenceHelper(context: Context) {

    private val mPrefs = context.getSharedPreferences("VITRINOVA", Context.MODE_PRIVATE)
    private val prefsLastSelectedCountry = "last_selected_country"

    /**
     * Kullanıcı daha önce bir ülke seçip generate yapmışsa ilgili bilgi repository'de bu
     * değere atanır ve sonraki açılışlarda kontrol edilir.
     * Eğer ilk defa uygulama açılmışsa default olarak "Switzerland" seçili gelir.
     */
    var userLastSelectedCountry: String?
        get() = mPrefs.getString(prefsLastSelectedCountry, "ch")
        set(value) = mPrefs.edit().putString(prefsLastSelectedCountry, value).apply()


}
