package com.furkanbozkurt.vitrinova.util

class Constants {
    companion object {
        const val REQUEST_CODE_TEXT_TO_SPEECH = 4314

        // Discover Page List Types
        const val FEATURED = "featured"
        const val NEW_PRODUCTS = "new_products"
        const val CATEGORIES = "categories"
        const val COLLECTIONS = "collections"
        const val EDITOR_SHOPS = "editor_shops"
        const val NEW_SHOPS = "new_shops"
    }
}