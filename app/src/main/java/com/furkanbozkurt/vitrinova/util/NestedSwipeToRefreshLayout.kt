package com.furkanbozkurt.vitrinova.util

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewConfiguration
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

/**
 *
 * SwipeToRefreshLayout içerisinde bulunan horizontal scroll edilebilen RecyclerView'lar ile
 * SwipeToRefreshLayout'un scroll statelerinin çakışmasının engelleyen custom view'dır
 *
 */

class NestedSwipeToRefreshLayout : SwipeRefreshLayout {
    private var mTouchSlop = 0
    private var mPrevX = 0f

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        mTouchSlop = ViewConfiguration.get(context).scaledTouchSlop
    }

    constructor(context: Context) : super(context) {}

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> mPrevX = MotionEvent.obtain(event).x
            MotionEvent.ACTION_MOVE -> {
                val eventX = event.x
                val xDiff = Math.abs(eventX - mPrevX)
                if (xDiff > mTouchSlop) {
                    return false
                }
            }
        }
        return super.onInterceptTouchEvent(event)
    }
}