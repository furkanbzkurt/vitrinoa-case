package com.furkanbozkurt.vitrinova.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.furkanbozkurt.vitrinova.BR
import com.furkanbozkurt.vitrinova.util.autoCleared
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
abstract class BaseFragment<VM : ViewModel, DB : ViewDataBinding> : Fragment() {

    var binding by autoCleared<DB>()
    val viewModel: VM by lazy { ViewModelProvider(this).get(getViewModelClass()) }

    protected abstract fun getViewModelClass(): Class<VM>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getContentView(), container, false)
        val view = binding.root
        setupUI()

        return view
    }

    abstract fun setupUI()

    abstract fun getContentView(): Int

    /**
     * Fragment içeriğinin state'ine  göre loading, success ve error durumlarında ilgili durumun
     * ilgili sayfanın layoutu üzerinde binding variabelar aracılığıyla gösterilmesini sağlar.
     * @param isLoading: Sayfa state'inin loading statei olup olmadığını ifade eder
     * @param errorMessage: Sadece error durumlarında dolu gelir.
     *
     */
    fun setLoadingState(
        isLoading: Boolean,
        errorMessage: String? = ""
    ) {
        binding.setVariable(BR.loadingState, isLoading)
        if (errorMessage?.isNotEmpty()!!) {
            binding.setVariable(BR.errorState, true)
            binding.setVariable(BR.errorMessage, errorMessage)
        } else {
            if (!isLoading) binding.setVariable(BR.contentFetchState, true)
            binding.setVariable(BR.errorState, false)
        }
    }
}